import java.util.Scanner;

public class TypeOfArray {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the length of array:");
        int lenOfArr=sc.nextInt();
        int[] arr=new int[lenOfArr];
        int c1=0,c2=0;
        System.out.println("Enter the elements of array:");
        for(int i=0;i<lenOfArr;i++){
            arr[i]=sc.nextInt();
            if(arr[i]%2==0)
                c1=c1+1;
            else
                c2=c2+1;
        }
        if(c1==lenOfArr)
            System.out.println("Even");
        else if(c2==lenOfArr)
            System.out.println("Odd");
        else
            System.out.println("Mixed");
    }
}
